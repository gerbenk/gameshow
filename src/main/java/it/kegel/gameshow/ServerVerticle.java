package it.kegel.gameshow;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.SockJSBridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

/**
 * Created by gerben on 19/08/15.
 */
public class ServerVerticle extends AbstractVerticle {

    public void start() {
        Router router = Router.router(vertx);
//        router.get("/host").handler(ctx -> ctx.response().sendFile("static/host.html"));
//        router.get("/client").handler(ctx -> ctx.response().sendFile("static/client.html"));
//        router.get("/host.js").handler(ctx -> ctx.response().sendFile("static/host.js"));
//        router.get("/client.js").handler(ctx -> ctx.response().sendFile("static/client.js"));
//        router.get("/background.jpg").handler(ctx -> ctx.response().sendFile("static/background.jpg"));
//        router.get("/background.png").handler(ctx -> ctx.response().sendFile("static/background.png"));
//        router.get("/redcross.png").handler(ctx -> ctx.response().sendFile("static/redcross.png"));
//        router.route().handler(ctx -> {
//            System.out.println(ctx.request().path());
//            ctx.next();
//        });
        router.mountSubRouter("/eventbus", SockJSHandler.create(vertx).bridge(new SockJSBridgeOptions()
                .addOutboundPermitted(new PermittedOptions().setAddress("gameshow.event"))
                .addOutboundPermitted(new PermittedOptions().setAddress("gameshow.cross"))
                .addInboundPermitted(new PermittedOptions().setAddress("gameshow.event"))
                .addInboundPermitted(new PermittedOptions().setAddress("gameshow.cross"))
        ));
        router.route("/client").handler(ctx -> ctx.response().sendFile("webroot/client.html"));
        router.route("/host").handler(ctx -> ctx.response().sendFile("webroot/host.html"));
        router.route().handler(StaticHandler.create("webroot/"));

        vertx.createHttpServer().requestHandler(router).listen(8081);
        System.out.println("Listening on port 8081");

        vertx.eventBus().consumer("gameshow.event", (Handler<Message<JsonObject>>) event -> {
            JsonObject obj = event.body();
            System.out.println(obj.getInteger("id") + " " + obj.getString("message"));
        });

        vertx.eventBus().consumer("gameshow.cross", (Handler<Message<JsonObject>>) event -> {
            JsonObject obj = event.body();
            System.out.println(obj.getInteger("id") + " cross");
        });
    }
}
