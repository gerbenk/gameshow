var eb = new vertx.EventBus(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/eventbus');

eb.onopen = function() {
    eb.registerHandler('gameshow.event', function (event) {
        $('#message' + event.id).text(event.message);
    });
    eb.registerHandler('gameshow.cross', function (event) {
        if (event.cross) {
            $('#cross' + event.id).show();
        } else {
            $('#cross' + event.id).hide();
        }
    });
}

